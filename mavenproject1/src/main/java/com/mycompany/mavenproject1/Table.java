/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

/**
 *
 * @author informatics
 */
public class Table {
    private char[][] board;

    public Table() {
        this.board = new char[3][3];
    }

    public void initializeBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = '-';
            }
        }
    }

    public void displayBoard() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

    public boolean move(Player player, int row, int col) {
        if (board[row][col] == '-') {
            board[row][col] = player.getSymbol();
            return true;
        }
        return false;
    }

    public char getBoardValue(int row, int col) {
        return board[row][col];
    }
}
