/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;
import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Lab4_2 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Player p1 = new Player("Player 1", 'X');
        Player p2 = new Player("Player 2", 'O');

        while (true) {
        Game game = new Game(p1, p2);
        game.startGame();

        while (true) {
            System.out.println(game.getCurrentPlayer().getName() + "'s turn. Enter row and column (1-3): ");
            int row = scanner.nextInt() - 1;
            int col = scanner.nextInt() - 1;
            if (game.getTable().move(game.getCurrentPlayer(), row, col)) {
                game.getTable().displayBoard();
                if (game.checkWin()) {
                    System.out.println(game.getCurrentPlayer().getName() + " wins!");
                    break;
                }
                game.changePlayer(p1, p2);
            } else {
                System.out.println("Invalid move. Try again.");
            }
            }

            System.out.println("Do you want to play again? (yes/no): ");
            String playAgain = scanner.next().toLowerCase();

            if (!playAgain.equals("yes")) {
                break;
            }
        }

        scanner.close();
    }
}
