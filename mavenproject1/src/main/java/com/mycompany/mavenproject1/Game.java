/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.mavenproject1;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {
    private Player currentPlayer;
    private Table table;

    public Game(Player p1, Player p2) {
        this.currentPlayer = p1;
        this.table = new Table();
    }

    public void startGame() {
        table.initializeBoard();
        table.displayBoard();
    }

    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (table.getBoardValue(i, 0) == currentPlayer.getSymbol() && table.getBoardValue(i, 1) == currentPlayer.getSymbol() && table.getBoardValue(i, 2) == currentPlayer.getSymbol()) {
                return true;
            }
        }

        for (int i = 0; i < 3; i++) {
            if (table.getBoardValue(0, i) == currentPlayer.getSymbol() && table.getBoardValue(1, i) == currentPlayer.getSymbol() && table.getBoardValue(2, i) == currentPlayer.getSymbol()) {
                return true;
            }
        }

        if (table.getBoardValue(0, 0) == currentPlayer.getSymbol() && table.getBoardValue(1, 1) == currentPlayer.getSymbol() && table.getBoardValue(2, 2) == currentPlayer.getSymbol()) {
            return true;
        }

        if (table.getBoardValue(2, 0) == currentPlayer.getSymbol() && table.getBoardValue(1, 1) == currentPlayer.getSymbol() && table.getBoardValue(0, 2) == currentPlayer.getSymbol()) {
            return true;
        }

        return false;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void changePlayer(Player p1, Player p2) {
        if (currentPlayer == p1) {
            currentPlayer = p2;
        } else {
            currentPlayer = p1;
        }
    }

    public Table getTable() {
        return table;
    }
}

